/* TnlpTol.cpp: API TOL ipopt

   Copyright (C) 2005-2011, Bayes Decision, SL (Spain [EU])

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
   USA.
 */

#include "TnlpTol.hpp"


BText TnlpTol::_MID_nlp("[TolIpopt::Nlp]");
int TnlpTol::badFields_ = false;


/////////////////////////////////////////////////////////////////////////////
BDat TnlpTol::code_addr( TnlpTol* ptr )
/////////////////////////////////////////////////////////////////////////////
{
  BDat addr = 0.0;
  *(TnlpTol**)(&addr) = ptr;
  return addr;
}

/////////////////////////////////////////////////////////////////////////////
TnlpTol* TnlpTol::decode_addr( BDat& addr )
/////////////////////////////////////////////////////////////////////////////
{
  return *((TnlpTol**)(&addr));
}

/////////////////////////////////////////////////////////////////////////////
BSyntaxObject* TnlpTol::EnsureMember(const BText& name)
/////////////////////////////////////////////////////////////////////////////
{
  BSyntaxObject* mbm = wrapper_.Member(name);
  if(!mbm)
  {
    Error(BText(MID()+"Unexpected meber name "+name));
    badFields_ = true;
    BGrammar::Turn_StopFlag_On(); 
  };
  return(mbm);
};

/////////////////////////////////////////////////////////////////////////////
TnlpTol::TnlpTol(BNameBlock& wrapper)
/////////////////////////////////////////////////////////////////////////////
: 
  isGood_(true),
  wrapper_(wrapper),
  n_(Dat(EnsureMember("n"))),
  x_lower(VMat(EnsureMember("x_lower"))),
  x0(VMat(EnsureMember("x0"))),
  x_upper(VMat(EnsureMember("x_upper"))),
  _id_status(Dat(EnsureMember("_.id_status"))),
  _co_status(Text(EnsureMember("_.co_status"))),
  _x(VMat(EnsureMember("_.x"))),
  _z_L(VMat(EnsureMember("_.z_L"))),
  _z_U(VMat(EnsureMember("_.z_U"))),
  _g(VMat(EnsureMember("_.g"))),
  _lambda(VMat(EnsureMember("_.lambda"))),
  _obj_value(Dat(EnsureMember("_.obj_value"))),
  _handler(Dat(EnsureMember("_.handler")))
{
  if(badFields_) 
  { 
    badFields_ = false;
    isGood_ = false;
    Error(MID()+" Error during creation of TnlpTol!\n");
    return; 
  }
  app = IpoptApplicationFactory();
  _handler = code_addr(this);
}


/////////////////////////////////////////////////////////////////////////////
/** Default destructor */
TnlpTol::~TnlpTol()
/////////////////////////////////////////////////////////////////////////////
{}


/////////////////////////////////////////////////////////////////////////////
/** Intialize the IpoptApplication and process the options */
int TnlpTol::initialize()
/////////////////////////////////////////////////////////////////////////////
{
  // Intialize the IpoptApplication and process the options
  ApplicationReturnStatus status;
  status = app->Initialize();
  if (status != Solve_Succeeded) 
  {
    isGood_ = false;
    Error(MID()+" Error during initialization!\n");
    return (int) status;
  }
}

/////////////////////////////////////////////////////////////////////////////
/** Method to return some info about the nlp */
bool TnlpTol::check_x_bounds(Index n)
/////////////////////////////////////////////////////////////////////////////
{
  bool ok = true;
  if(!n)
  {
    Error(MID()+" Number of variables must be at least one.");
    ok = false;
  }
  if((x_lower.Rows()!=n)|| (x_lower.Columns()!=1))
  {
    Error(MID()+" x_lower matrix should be ("+n+"x"+1+") instead of ("+
          x_lower.Rows()+"x"+x_lower.Columns()+")");
    ok = false;
  }
  if((x_upper.Rows()!=n)|| (x_upper.Columns()!=1))
  {
    Error(MID()+" x_upper matrix should be ("+n+"x"+1+") instead of ("+
          x_upper.Rows()+"x"+x_upper.Columns()+")");
    ok = false;
  }
  return(ok);
}

/////////////////////////////////////////////////////////////////////////////
/** Method to return the x-bounds for my problem */
bool TnlpTol::get_x_bounds_info(Index n, Number* x_l, Number* x_u)
/////////////////////////////////////////////////////////////////////////////
{
  for (Index i=0; i<n; i++) {
    x_l[i] = x_lower.GetCell(i,0);
    x_u[i] = x_upper.GetCell(i,0);
  }
  return(true);
}

/////////////////////////////////////////////////////////////////////////////
/** Method to return the starting point for the algorithm */
bool TnlpTol::get_starting_point(Index n, bool init_x, Number* x,
                                 bool init_z, Number* z_L, Number* z_U,
                                 Index m, bool init_lambda,
                                 Number* lambda)
/////////////////////////////////////////////////////////////////////////////
{
  if (!init_x || init_z || init_lambda) {
    return false;
  }
  if(init_x && ((x0.Rows()!=n)|| (x0.Columns()!=1)))
  {
    Error(MID()+" Needed initial values x0 matrix should be ("+n+"x"+1+") instead of ("+
          x0.Rows()+"x"+x0.Columns()+")");
    return(false);
  }
  if(!init_x && x0.Rows() && ((x0.Rows()!=n)|| (x0.Columns()!=1)))
  {
    Warning(MID()+" Initial values x0 matrix should be ("+n+"x"+1+") instead of ("+
          x0.Rows()+"x"+x0.Columns()+") but it's not necessary use them.");
    return(false);
  }
  // set the starting point
  for (Index i=0; i<n; i++) {
    x[i] = x0.GetCell(i,0);
  }
  return(true);
}

/////////////////////////////////////////////////////////////////////////////
bool TnlpTol::set_dense(int n, const Number* x, BVMat& xd)
/////////////////////////////////////////////////////////////////////////////
{
  xd.BlasRDense(n,1);
  double* x_;
  int nzmax;
  xd.StoredData(x_, nzmax);
  if(nzmax!=n) { return(false); }
  memcpy(x_, x, n*sizeof(double));
  return(true);
}

/////////////////////////////////////////////////////////////////////////////
bool TnlpTol::get_dense(int n, Number* x, const BVMat& xd)
/////////////////////////////////////////////////////////////////////////////
{
  const double* x_;
  int nzmax;
  if(xd.Code()==BVMat::ESC_blasRdense)
  {
    xd.StoredData(x_, nzmax);
    if(nzmax!=n) { return(false); }
    memcpy(x, x_, n*sizeof(double));
  }
  else
  {
    BVMat dense;
    dense.Convert(xd,BVMat::ESC_blasRdense);
    return(get_dense(n,x,dense));
  }
  return(true);
}

/////////////////////////////////////////////////////////////////////////////
bool TnlpTol::get_triplet_structure(int nn, Index* iRow, Index* jCol, const BVMat& t)
/////////////////////////////////////////////////////////////////////////////
{
  if(t.Code()==BVMat::ESC_chlmRtriplet)
  {
    BMatrix<double> ijx;
    t.GetTriplet(ijx);
    if(ijx.Rows()!=nn)
    {
      return(false);
    }    
    Index k;
    for(k=0; k<nn; k++)
    {
      iRow[k] = (int)ijx(k,0);
      jCol[k] = (int)ijx(k,1);
    }
  }
  else
  {
    BVMat triplet;
    triplet.Convert(t,BVMat::ESC_chlmRtriplet);
    return(get_triplet_structure(nn,iRow,jCol,triplet));
  }
  return(false);
}

/////////////////////////////////////////////////////////////////////////////
bool TnlpTol::get_triplet_values(int nn, Number* x, const BVMat& t)
/////////////////////////////////////////////////////////////////////////////
{
  if(t.Code()==BVMat::ESC_chlmRtriplet)
  {
    BMatrix<double> ijx;
    t.GetTriplet(ijx);
    if(ijx.Rows()!=nn)
    {
      return(false);
    }    
    Index k;
    for(k=0; k<nn; k++)
    {
      x[k] = ijx(k,2);
    }
  }
  else
  {
    BVMat triplet;
    triplet.Convert(t,BVMat::ESC_chlmRtriplet);
    return(get_triplet_values(nn,x,triplet));
  }
  return(false);
}


/////////////////////////////////////////////////////////////////////////////
/** This method is called when the algorithm is complete so the TNLP can store/write the solution */
void TnlpTol::finalize_solution(SolverReturn status,
                                Index n, const Number* x, const Number* z_L, const Number* z_U,
                                Index m, const Number* g, const Number* lambda,
                                Number obj_value,
              				          const IpoptData* ip_data,
			                          IpoptCalculatedQuantities* ip_cq)
/////////////////////////////////////////////////////////////////////////////
{
  _id_status = status;
  set_dense(n, x, _x);
  set_dense(n, z_L, _z_L);
  set_dense(n, z_U, _z_U);
  set_dense(m, g, _g);
  set_dense(m, lambda, _lambda);
  double _obj_value = obj_value;
  if(status=SUCCESS) { _co_status="SUCCESS"; }
  if(status=MAXITER_EXCEEDED) { _co_status="MAXITER_EXCEEDED"; }
  if(status=CPUTIME_EXCEEDED) { _co_status="CPUTIME_EXCEEDED"; }
  if(status=STOP_AT_TINY_STEP) { _co_status="STOP_AT_TINY_STEP"; }
  if(status=STOP_AT_ACCEPTABLE_POINT) { _co_status="STOP_AT_ACCEPTABLE_POINT"; }
  if(status=LOCAL_INFEASIBILITY) { _co_status="LOCAL_INFEASIBILITY"; }
  if(status=USER_REQUESTED_STOP) { _co_status="USER_REQUESTED_STOP"; }
  if(status=FEASIBLE_POINT_FOUND) { _co_status="FEASIBLE_POINT_FOUND"; }
  if(status=DIVERGING_ITERATES) { _co_status="DIVERGING_ITERATES"; }
  if(status=RESTORATION_FAILURE) { _co_status="RESTORATION_FAILURE"; }
  if(status=ERROR_IN_STEP_COMPUTATION) { _co_status="ERROR_IN_STEP_COMPUTATION"; }
  if(status=INVALID_NUMBER_DETECTED) { _co_status="INVALID_NUMBER_DETECTED"; }
  if(status=TOO_FEW_DEGREES_OF_FREEDOM) { _co_status="TOO_FEW_DEGREES_OF_FREEDOM"; }
  if(status=INVALID_OPTION) { _co_status="INVALID_OPTION"; }
  if(status=OUT_OF_MEMORY) { _co_status="OUT_OF_MEMORY"; }
  if(status=INTERNAL_ERROR) { _co_status="INTERNAL_ERROR"; }
  Std(BText("\n")+MID()+"\tFinal status="+_co_status+"\tobj_value="+obj_value);
}

/////////////////////////////////////////////////////////////////////////////
bool TnlpTol::intermediate_callback(AlgorithmMode mode,
                                    Index iter, Number obj_value,
                                    Number inf_pr, Number inf_du,
                                    Number mu, Number d_norm,
                                    Number regularization_size,
                                    Number alpha_du, Number alpha_pr,
                                    Index ls_trials,
                                    const IpoptData* ip_data,
                                    IpoptCalculatedQuantities* ip_cq)
/////////////////////////////////////////////////////////////////////////////
{
  BText phase = "";
  if(mode==RegularMode)          { phase = "Regular"; }
  if(mode==RestorationPhaseMode) { phase = "Restoration"; }
  Std(BText("\n")+MID()+"\tphase:"+phase+ "\titer="+iter+"\tobj_value="+obj_value);
  if(BGrammar::StopFlag()) { return(false); }
  return(true);
}


