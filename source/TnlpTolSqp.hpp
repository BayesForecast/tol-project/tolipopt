/* TnlpTolSqp.hpp: API TOL ipopt
   M�s detalles del paquete en http://ab-initio.mit.edu/wiki/index.php/NLopt

   Copyright (C) 2005-2011, Bayes Decision, SL (Spain [EU])

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
   USA.
 */

#ifndef _TnlpTolSqp_

#define _TnlpTolSqp_


#if defined(_MSC_VER)
#  include <win_tolinc.h>
#endif

#include "TnlpTol.hpp"



///////////////////////////////////////////////////////////////////////////////
class TnlpTolSqp : public TnlpTol
///////////////////////////////////////////////////////////////////////////////
{
protected:
  static BText _MID_sqp;
  static BText& MID() { return(_MID_sqp); }
public:
/*
  Solves the quadratic problem

  min x' C x + c' x + c0
  subject to 
    l_A <= A x <= u_A 
    l_x <=   x <= u_x 

  If C and A are sparse matrices methods will be more efficient
*/

  //Input data
  const BVMat& C;
  const BVMat& c;
  const BDat& c0;

  const BVMat& A_lower;
  const BVMat& A;
  const BVMat& A_upper;

  //Auxiliar data
  BVMat A_triplet;
  BVMat C_triplet;


  TnlpTolSqp(BNameBlock& wrapper);
  virtual ~TnlpTolSqp();
  bool get_nlp_info(Index& n, Index& m, Index& nnz_jac_g,
                    Index& nnz_h_lag, IndexStyleEnum& index_style);
  bool get_bounds_info(Index n, Number* x_l, Number* x_u,
                       Index m, Number* g_l, Number* g_u);
  bool eval_f(Index n, const Number* x, bool new_x, Number& obj_value);
  bool eval_grad_f(Index n, const Number* x, bool new_x, Number* grad_f);
  bool eval_g(Index n, const Number* x, bool new_x, Index m, Number* g);
  bool eval_jac_g(Index n, const Number* x, bool new_x,
                  Index m, Index nele_jac, Index* iRow, Index *jCol,
                  Number* values);
  bool eval_h(Index n, const Number* x, bool new_x,
              Number obj_factor, Index m, const Number* lambda,
              bool new_lambda, Index nele_hess, Index* iRow,
              Index* jCol, Number* values);

private:
  /**@name Methods to block default compiler methods.
   * The compiler automatically generates the following three methods.
   *  Since the default compiler implementation is generally not what
   *  you want (for all but the most simple classes), we usually 
   *  put the declarations of these methods in the private section
   *  and never implement them. This prevents the compiler from
   *  implementing an incorrect "default" behavior without us
   *  knowing. (See Scott Meyers book, "Effective C++")
   *  
   */
  //@{
  TnlpTolSqp();
  TnlpTolSqp(const TnlpTolSqp&);
  TnlpTolSqp& operator=(const TnlpTolSqp&);
  //@}
};


#endif /*_TnlpTolSqp_*/