/* TnlpTol.cpp: API TOL ipopt

   Copyright (C) 2005-2011, Bayes Decision, SL (Spain [EU])

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
   USA.
 */

#include "JournalTol.hpp"

namespace Ipopt {

TolJournal::TolJournal(
  const std::string& name,
  EJournalLevel default_level
)
:
 Journal(name, default_level),
 buffer_(1024*1024)
{
}

void TolJournal::PrintImpl(EJournalCategory category, EJournalLevel level,
                           const char* str)
{
//DBG_START_METH("TolJournal::PrintImpl", 0);
  Std(str);
}

void TolJournal::PrintfImpl(EJournalCategory category, EJournalLevel level,
                            const char* pformat, va_list ap)
{
//DBG_START_METH("TolJournal::PrintfImpl", 0);
  int count_chars = vsprintf(buffer_.Buffer(), pformat, ap);
  if(count_chars<0)
  {
    Error("[TolJournal::PrintfImpl] Unhandled error calling vsprintf");
  }
  else if(count_chars>=buffer_.Size()-1)
  {
    Error(BText("[TolJournal::PrintfImpl] Too large message calling vsprintf count_chars=")+count_chars+">="+(buffer_.Size()-1));
  }
  else
  {
    Std(buffer_);
  }
}

void TolJournal::FlushBufferImpl()
{
}

};
