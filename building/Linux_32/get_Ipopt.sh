#!/bin/sh

cd $(dirname $0)

# Set the following to the latest IPOPT version.
#  THERE MUST BE NO SPACE BEFORE AND AFTER THE EQUAL (=) OPERATOR.
ipopt_ver=3.12.4

set -e

wgetcmd=wget
wgetcount=`which wget 2>/dev/null | wc -l`
if test ! $wgetcount = 1; then
  echo "Utility wget not found in your PATH."
  if test `uname` = Darwin; then
    wgetcmd=ftp
    echo "Using ftp command instead."
  elif test `uname` = FreeBSD; then
    wgetcmd=fetch
    echo "Using fetch command instead."
  else
    exit -1
  fi
fi

echo " "
echo "Running script for downloading the source code for Ipopt"
echo " "

rm -f Ipopt-*.tgz

echo "Downloading the source code from ..."
$wgetcmd http://www.coin-or.org/download/source/Ipopt/Ipopt-${ipopt_ver}.tgz

echo "Unpacking the source code..."
tar -xvf Ipopt-${ipopt_ver}.tgz

echo "Deleting the tgz file..."
rm Ipopt-${ipopt_ver}.tgz

rm -rf Ipopt
mv Ipopt-${ipopt_ver} Ipopt

echo " "
echo "Done downloading the source code for Ipopt."
echo " "
echo "Verify that there are no error message in the output above."



